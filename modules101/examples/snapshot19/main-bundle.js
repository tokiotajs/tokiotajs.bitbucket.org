(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	factory();
}(this, function () { 'use strict';

	var enums = {
		breeds : { elf : 'elf',	dwarf: 'dwarf',	human: 'human', giant: 'giant',	orc: 'orc',	troll: 'troll',	goblin: 'goblin' }, 
		classes : { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer',	cleric: 'cleric' }};

	function Breed(breedname){
	  this.name = breedname;
		this.toString = function () { return this.name; };
	}

	function Class(classname){
		this.name = classname;
		this.toString = function () { return this.name; };
	}

	function Character(name, classname, breedname){
		this.name = name;
		this.class = new Class(classname);
		this.breed = new Breed(breedname);
		this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
	}

	var char = new Character('Eldelbar', enums.classes.warrior, enums.breeds.elf);
	var output = document.getElementById("output");
	output.innerHTML = char;

}));