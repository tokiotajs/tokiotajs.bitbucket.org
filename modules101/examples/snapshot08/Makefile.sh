#!/bin/bash

# clean generated files
rm -f main-bundle*

# Depends on having webpack command installed, with the command:
# npm install webpack -g
webpack main.js main-bundle.js

# Depends on having uglifyjs command installed, with the command:
# npm install uglifyjs -g
uglifyjs main-bundle.js --compress --mangle -o main-bundle-min.js
