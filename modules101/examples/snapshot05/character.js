Game.Character = function (name, classname, breedname) {
	this.name = name;
	this.class = new Game.Class(classname);
	this.breed = new Game.Breed(breedname);
	this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
};
