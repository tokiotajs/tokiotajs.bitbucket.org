Minification

 * Reduces bundle size,
 * Keeps code runnable.
 * Using [UglifyJS](http://lisperator.net/uglifyjs/)
