Game = {};
Game.enums = {
	breeds: { elf: 'elf', dwarf: 'dwarf', human: 'human', giant: 'giant', orc: 'orc', troll: 'troll', goblin: 'goblin' },
	classes: { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer', cleric: 'cleric' }
};
Game.Breed = function (breedname) {
  this.name = breedname;
  this.toString = function () { return this.name; };
};
Game.Class = function (classname) {
  this.name = classname;
  this.toString = function () { return this.name; };
};
Game.Character = function (name, classname, breedname) {
  this.name = name;
  this.class = new Game.Class(classname);
  this.breed = new Game.Breed(breedname);
  this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
};
