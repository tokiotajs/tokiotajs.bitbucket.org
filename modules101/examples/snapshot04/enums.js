Game.enums = {
	breeds: { elf: 'elf', dwarf: 'dwarf', human: 'human', giant: 'giant', orc: 'orc', troll: 'troll', goblin: 'goblin' },
	classes: { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer', cleric: 'cleric' }
};
