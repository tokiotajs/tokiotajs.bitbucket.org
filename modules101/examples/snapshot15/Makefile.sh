#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having webpack command installed, with the command:
# npm install webpack -g
# You will also need babel-loader as a development dependency:
# npm install babel-loader -D

# Also depends on having babelify installed locally
# npm install babelify

webpack main.js main-bundle.js