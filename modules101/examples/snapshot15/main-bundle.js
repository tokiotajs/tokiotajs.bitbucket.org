/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _enums = __webpack_require__(1);

	var _enums2 = _interopRequireDefault(_enums);

	var _character = __webpack_require__(2);

	var _character2 = _interopRequireDefault(_character);

	var char = new _character2['default']('Eldelbar', _enums2['default'].classes.alchemist, _enums2['default'].breeds.elf);
	var output = document.getElementById('output');
	output.innerHTML = char;

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	var enums = {
		breeds: { elf: 'elf', dwarf: 'dwarf', human: 'human', giant: 'giant', orc: 'orc', troll: 'troll', goblin: 'goblin' },
		classes: { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer', cleric: 'cleric' }
	};

	exports['default'] = enums;
	module.exports = exports['default'];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	exports['default'] = Character;

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _breed = __webpack_require__(3);

	var _breed2 = _interopRequireDefault(_breed);

	var _class = __webpack_require__(4);

	var _class2 = _interopRequireDefault(_class);

	function Character(name, classname, breedname) {
		this.name = name;
		this['class'] = new _class2['default'](classname);
		this.breed = new _breed2['default'](breedname);
		this.toString = function () {
			return this.name + ', ' + this['class'] + ' ' + this.breed;
		};
	}

	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports["default"] = Breed;

	function Breed(breedname) {
		this.name = breedname;
		this.toString = function () {
			return this.name;
		};
	}

	module.exports = exports["default"];

/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports["default"] = Class;

	function Class(classname) {
		this.name = classname;
		this.toString = function () {
			return this.name;
		};
	}

	module.exports = exports["default"];

/***/ }
/******/ ]);