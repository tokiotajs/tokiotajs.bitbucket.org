#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having browserify command installed, with the command:
# npm install browserify -g

# Also depends on having es6ify installed locally
# npm install es6ify
browserify -t es6ify main.js -o main-bundle.js
