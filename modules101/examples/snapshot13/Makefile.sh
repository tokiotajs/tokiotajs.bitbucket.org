#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having browserify command installed, with the command:
# npm install browserify -g

# Also depends on having deamdify installed locally
# npm install deamdify
browserify -t deamdify main.js -o main-bundle.js