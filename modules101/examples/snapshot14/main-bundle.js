(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports["default"] = Breed;

function Breed(breedname) {
	this.name = breedname;
	this.toString = function () {
		return this.name;
	};
}

module.exports = exports["default"];

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});
exports['default'] = Character;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _breed = require('./breed');

var _breed2 = _interopRequireDefault(_breed);

var _class = require('./class');

var _class2 = _interopRequireDefault(_class);

function Character(name, classname, breedname) {
	this.name = name;
	this['class'] = new _class2['default'](classname);
	this.breed = new _breed2['default'](breedname);
	this.toString = function () {
		return this.name + ', ' + this['class'] + ' ' + this.breed;
	};
}

module.exports = exports['default'];

},{"./breed":1,"./class":3}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports["default"] = Class;

function Class(classname) {
	this.name = classname;
	this.toString = function () {
		return this.name;
	};
}

module.exports = exports["default"];

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});
var enums = {
	breeds: { elf: 'elf', dwarf: 'dwarf', human: 'human', giant: 'giant', orc: 'orc', troll: 'troll', goblin: 'goblin' },
	classes: { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer', cleric: 'cleric' }
};

exports['default'] = enums;
module.exports = exports['default'];

},{}],5:[function(require,module,exports){
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _enums = require('./enums');

var _enums2 = _interopRequireDefault(_enums);

var _character = require('./character');

var _character2 = _interopRequireDefault(_character);

var char = new _character2['default']('Eldelbar', _enums2['default'].classes.alchemist, _enums2['default'].breeds.elf);
var outputElement = document.getElementById('output');
outputElement.innerHTML = char;

},{"./character":2,"./enums":4}]},{},[5]);
